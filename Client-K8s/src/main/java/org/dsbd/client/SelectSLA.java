package org.dsbd.client;

import java.util.Collections;
import javax.swing.JOptionPane;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class SelectSLA extends javax.swing.JPanel {
    private String token;
    private String urlSlamanager = "http://localhost:30085/";
    private final RestTemplate restTemplate = new RestTemplate();
    private String[] descriptions;
    private Float[] seasonalities;
    
    public SelectSLA(String token) {
        this.token = token;
        initComponents();
        myInitComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        lblSLA = new javax.swing.JLabel();
        lblMax = new javax.swing.JLabel();
        btnAdd = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();
        lblPromQL = new javax.swing.JLabel();
        lblMin = new javax.swing.JLabel();
        sMin = new javax.swing.JSlider();
        sMax = new javax.swing.JSlider();
        spnMax = new javax.swing.JSpinner();
        spnMin = new javax.swing.JSpinner();
        lblDescription = new javax.swing.JLabel();
        tfDescription = new javax.swing.JTextField();
        cbPromQL = new javax.swing.JComboBox<>();
        tfName = new javax.swing.JTextField();
        lblSeasonality = new javax.swing.JLabel();
        spnSeasonality = new javax.swing.JSpinner();
        jLabel1 = new javax.swing.JLabel();

        jButton1.setText("jButton1");

        lblSLA.setText("SLA Name");

        lblMax.setText("Maximum Value");

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnRefresh.setText("Refresh SLA List");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        lblPromQL.setText("PromQL");

        lblMin.setText("Minimum Value");

        sMin.setMaximum(10000);
        sMin.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sMinStateChanged(evt);
            }
        });

        sMax.setMaximum(10000);
        sMax.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sMaxStateChanged(evt);
            }
        });

        spnMax.setModel(new javax.swing.SpinnerNumberModel(0.0f, 0.0f, null, 1.0f));
        spnMax.setMaximumSize(new java.awt.Dimension(65, 22));
        spnMax.setMinimumSize(new java.awt.Dimension(65, 22));

        spnMin.setModel(new javax.swing.SpinnerNumberModel(0.0f, 0.0f, null, 1.0f));
        spnMin.setMaximumSize(new java.awt.Dimension(65, 22));
        spnMin.setMinimumSize(new java.awt.Dimension(65, 22));

        lblDescription.setText("Description");

        tfDescription.setEditable(false);

        cbPromQL.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbPromQLItemStateChanged(evt);
            }
        });

        lblSeasonality.setText("Seasonality (min.)");

        spnSeasonality.setModel(new javax.swing.SpinnerNumberModel(0.0f, 0.0f, null, 1.0f));

        jLabel1.setText("Leave 0 to set to non-seasonality.");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblSeasonality)
                        .addGap(18, 18, 18)
                        .addComponent(spnSeasonality, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblSLA)
                            .addComponent(lblMin)
                            .addComponent(lblPromQL)
                            .addComponent(lblMax)
                            .addComponent(lblDescription))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(cbPromQL, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(tfDescription)
                                    .addComponent(tfName, javax.swing.GroupLayout.Alignment.LEADING)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnRefresh)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnAdd))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(sMax, javax.swing.GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE)
                                            .addComponent(sMin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(spnMax, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(spnMin, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                        .addGap(30, 30, 30))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPromQL)
                    .addComponent(cbPromQL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDescription)
                    .addComponent(tfDescription))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSLA)
                    .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMin)
                    .addComponent(spnMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMax)
                    .addComponent(spnMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSeasonality)
                    .addComponent(spnSeasonality, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnRefresh))
                .addGap(57, 57, 57))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void myInitComponents(){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + token);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        try {
            ResponseEntity<String> response = restTemplate.exchange(urlSlamanager + "customMetricList", HttpMethod.GET, entity, String.class);
            JSONArray responseJSON = new JSONArray(response.getBody());
            cbPromQL.removeAllItems();
            int length = responseJSON.length();
            descriptions = new String[length];
            seasonalities = new Float[length];
            System.out.println(response.getBody());
            int i = 0;
            for(Object metric : responseJSON){
                JSONObject jsonObject = new JSONObject(metric.toString());
                descriptions[i] = jsonObject.getString("description");
                seasonalities[i] = jsonObject.getFloat("seasonality")/60;
                cbPromQL.addItem(jsonObject.getString("promQL"));
                i++;
            }
            tfDescription.setText(descriptions[0]);
            spnSeasonality.setValue(seasonalities[0]);
        } catch (HttpClientErrorException ex) {
            JOptionPane.showMessageDialog(null, ex.getResponseBodyAsString(), "Warning", JOptionPane.WARNING_MESSAGE);
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }
    
    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        JSONObject slaJSON = new JSONObject();
        Float seasonality = Float.parseFloat(spnSeasonality.getValue().toString()) * 60;
        slaJSON.put("name", tfName.getText());
        slaJSON.put("promql", cbPromQL.getSelectedItem());
        slaJSON.put("description", tfDescription.getText());
        slaJSON.put("min", spnMin.getValue());
        slaJSON.put("max", spnMax.getValue());
        slaJSON.put("seasonality", seasonality);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + token);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> messageBody = new HttpEntity<>(slaJSON.toString(), headers);
        try {
            ResponseEntity<String> response = restTemplate.postForEntity(urlSlamanager + "create", messageBody, String.class);
            JOptionPane.showMessageDialog(null, response.getBody(), "Success", JOptionPane.INFORMATION_MESSAGE);
        } catch (HttpClientErrorException ex) {
            JOptionPane.showMessageDialog(null, ex.getResponseBodyAsString(), "Warning", JOptionPane.WARNING_MESSAGE);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "No response.", "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        myInitComponents();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void sMinStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sMinStateChanged
        spnMin.setValue((Object)sMin.getValue());
    }//GEN-LAST:event_sMinStateChanged

    private void sMaxStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sMaxStateChanged
        spnMax.setValue((Object)sMax.getValue());  
    }//GEN-LAST:event_sMaxStateChanged

    private void cbPromQLItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbPromQLItemStateChanged
        if(cbPromQL.getSelectedIndex() >= 0){
            tfDescription.setText(descriptions[cbPromQL.getSelectedIndex()]);
            spnSeasonality.setValue(seasonalities[cbPromQL.getSelectedIndex()]);
        }
    }//GEN-LAST:event_cbPromQLItemStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JComboBox<String> cbPromQL;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblMax;
    private javax.swing.JLabel lblMin;
    private javax.swing.JLabel lblPromQL;
    private javax.swing.JLabel lblSLA;
    private javax.swing.JLabel lblSeasonality;
    private javax.swing.JSlider sMax;
    private javax.swing.JSlider sMin;
    private javax.swing.JSpinner spnMax;
    private javax.swing.JSpinner spnMin;
    private javax.swing.JSpinner spnSeasonality;
    private javax.swing.JTextField tfDescription;
    private javax.swing.JTextField tfName;
    // End of variables declaration//GEN-END:variables
}
