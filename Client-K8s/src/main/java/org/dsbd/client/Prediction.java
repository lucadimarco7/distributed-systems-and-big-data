package org.dsbd.client;

import java.util.Base64;
import java.util.Collections;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.SpinnerModel;
import javax.swing.table.DefaultTableModel;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class Prediction extends javax.swing.JPanel {
    private String token;
    private String urlSlamanager = "http://localhost:30085/";
    private final RestTemplate restTemplate = new RestTemplate();
    private final Home component;

    public Prediction(String token, Home home) {
        this.token = token;
        this.component = home;
        initComponents();
        myInitComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblSLA = new javax.swing.JLabel();
        lblMinutes = new javax.swing.JLabel();
        btnPredict = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();
        cbSLA = new javax.swing.JComboBox<>();
        spnMinutes = new javax.swing.JSpinner();
        lblGraph = new javax.swing.JLabel();
        lblProb = new javax.swing.JLabel();
        tfProb = new javax.swing.JTextField();

        lblSLA.setText("SLA");

        lblMinutes.setText("Minutes of Prediction");

        btnPredict.setText("Predict");
        btnPredict.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPredictActionPerformed(evt);
            }
        });

        btnRefresh.setText("Refresh SLA List");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        spnMinutes.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        spnMinutes.setMaximumSize(new java.awt.Dimension(65, 22));
        spnMinutes.setMinimumSize(new java.awt.Dimension(65, 22));
        spnMinutes.setPreferredSize(new java.awt.Dimension(65, 22));

        lblProb.setText("Violation Probability");
        lblProb.setVisible(false);

        tfProb.setVisible(false);
        tfProb.setEditable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblGraph, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblProb)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfProb, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblSLA)
                                .addGap(18, 18, 18)
                                .addComponent(cbSLA, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblMinutes)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(spnMinutes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 155, Short.MAX_VALUE)
                                .addComponent(btnRefresh)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnPredict)))
                        .addGap(30, 30, 30))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblSLA)
                    .addComponent(cbSLA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMinutes)
                    .addComponent(spnMinutes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRefresh)
                    .addComponent(btnPredict))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblProb)
                    .addComponent(tfProb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addComponent(lblGraph, javax.swing.GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        myInitComponents();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnPredictActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPredictActionPerformed
        JSONObject predictJSON = new JSONObject();
        predictJSON.put("sla", cbSLA.getSelectedItem());
        predictJSON.put("minutes", spnMinutes.getValue());
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + token);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> messageBody = new HttpEntity<>(predictJSON.toString(), headers);
        try {
            ResponseEntity<String> response = restTemplate.postForEntity(urlSlamanager + "prediction", messageBody, String.class);
            JSONObject responseJSON = new JSONObject(response.getBody());
            String base64Image = responseJSON.getString("image");
            byte[] imageBytes = Base64.getDecoder().decode(base64Image);
            ImageIcon icon = new ImageIcon(imageBytes);
            lblGraph.setIcon(icon);
            lblProb.setVisible(true);
            tfProb.setText(responseJSON.get("prob_violation").toString()+"%");
            tfProb.setVisible(true);
            component.pack();
            component.setLocationRelativeTo(null);
        } catch (HttpClientErrorException  ex) {
            JOptionPane.showMessageDialog(null, ex.getResponseBodyAsString(), "Warning", JOptionPane.WARNING_MESSAGE);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "No response.", "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnPredictActionPerformed

    private void myInitComponents(){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + token);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        try {
            ResponseEntity<String> response = restTemplate.exchange(urlSlamanager + "slaList", HttpMethod.GET, entity, String.class);
            JSONArray responseArray = new JSONArray(response.getBody());
            cbSLA.removeAllItems();
            System.out.println(response.getBody());
            for(Object sla : responseArray){
                JSONObject jsonObject = new JSONObject(sla.toString());
                cbSLA.addItem(jsonObject.getString("sla"));
            }
        } catch (HttpClientErrorException ex) {
            JOptionPane.showMessageDialog(null, ex.getResponseBodyAsString(), "Warning", JOptionPane.WARNING_MESSAGE);
            cbSLA.removeAllItems();
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPredict;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JComboBox<String> cbSLA;
    private javax.swing.JLabel lblGraph;
    private javax.swing.JLabel lblMinutes;
    private javax.swing.JLabel lblProb;
    private javax.swing.JLabel lblSLA;
    private javax.swing.JSpinner spnMinutes;
    private javax.swing.JTextField tfProb;
    // End of variables declaration//GEN-END:variables
}
