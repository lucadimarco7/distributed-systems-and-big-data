package org.dsbd.client;

public class Admin extends javax.swing.JPanel {
    private String token;
    private String username;
    private Home home;
    
    public Admin(String token, String username, Home home) {
        this.token = token;
        this.username = username;
        this.home = home;
        initComponents();
        myInitComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 597, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 467, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void myInitComponents(){
        jTabbedPane1.add("Create New SLA", new CreateSLA(token));
        jTabbedPane1.add("Select New SLA", new SelectSLA(token));
        jTabbedPane1.add("Queries List", new QueriesList(token));
        jTabbedPane1.add("SLA List", new SLAList(token));
        jTabbedPane1.add("Violations", new Violations(token));
        jTabbedPane1.add("Violation Prediction", new Prediction(token,home));
        jTabbedPane1.add("Metrics List", new MetricsList(token));
        jTabbedPane1.add("Update Credentials", new UpdateAdmin(token, username));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables
}
