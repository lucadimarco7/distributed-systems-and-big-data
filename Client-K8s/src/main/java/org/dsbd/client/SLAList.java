package org.dsbd.client;

import java.util.Collections;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class SLAList extends javax.swing.JPanel {
    private String token;
    private String urlSlamanager = "http://localhost:30085/";
    private final RestTemplate restTemplate = new RestTemplate();

    public SLAList(String token) {
        this.token = token;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnRefresh = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblSLAList = new javax.swing.JTable();
        btnDelete = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();

        btnRefresh.setText("Refresh");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        tblSLAList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "SLA", "PromQL", "Minimum Value", "Maximum Value", "Seasonality", "Actual Value", "Violation Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                // Rendi editabili la seconda (column == 1) e la terza (column == 2) colonna
                return column == 1 || column == 2 || column == 3 || column == 4;
            }
        });
        tblSLAList.setRowHeight(25);
        tblSLAList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(tblSLAList);

        btnDelete.setBackground(new java.awt.Color(255, 102, 153));
        btnDelete.setForeground(java.awt.SystemColor.activeCaptionText);
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnUpdate.setText("Update Selected");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUpdate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelete))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 595, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRefresh)
                    .addComponent(btnDelete)
                    .addComponent(btnUpdate))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + token);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        DefaultTableModel model = (DefaultTableModel) tblSLAList.getModel();
        try {
            ResponseEntity<String> response = restTemplate.exchange(urlSlamanager + "slaList", HttpMethod.GET, entity, String.class);
            JSONArray responseArray = new JSONArray(response.getBody());
            model.setRowCount(0);
            System.out.println(response.getBody());
            for(Object sla : responseArray){
                JSONObject jsonObject = new JSONObject(sla.toString());
                response = restTemplate.exchange(urlSlamanager + "status/" + jsonObject.getString("sla"), HttpMethod.GET, entity, String.class);
                JSONObject jsonObject2 = new JSONObject(response.getBody());
                Object[] row = {jsonObject.getString("sla"), jsonObject.getString("promql"), jsonObject.getFloat("min"), jsonObject.getFloat("max"), jsonObject.getFloat("seasonality"), jsonObject2.getString("value"), jsonObject2.getBoolean("violationStatus")};
                model.addRow(row);
            }
        } catch (HttpClientErrorException ex) {
            JOptionPane.showMessageDialog(null, ex.getResponseBodyAsString(), "Warning", JOptionPane.WARNING_MESSAGE);
            model.setRowCount(0);
        }
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedRow = tblSLAList.getSelectedRow();
        if (selectedRow != -1) {
            Object value = tblSLAList.getValueAt(selectedRow, 0);
            JSONObject deleteJSON = new JSONObject();
            deleteJSON.put("name", value);
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + token);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            HttpEntity<String> messageBody = new HttpEntity<>(deleteJSON.toString(), headers);
            try{
                ResponseEntity<String> response = restTemplate.exchange(urlSlamanager + "delete", HttpMethod.DELETE, messageBody, String.class);
                JOptionPane.showMessageDialog(null, response.getBody(), "Success", JOptionPane.INFORMATION_MESSAGE);
                btnRefreshActionPerformed(evt);
            } catch (HttpClientErrorException ex) {
                JOptionPane.showMessageDialog(null, ex.getResponseBodyAsString(), "Warning", JOptionPane.WARNING_MESSAGE);
                btnRefreshActionPerformed(evt);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "No response.", "Warning", JOptionPane.WARNING_MESSAGE);
                btnRefreshActionPerformed(evt);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No row selected.");
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        int selectedRow = tblSLAList.getSelectedRow();
        if (selectedRow != -1) {
            JSONObject slaJSON = new JSONObject();
            Float seasonality = Float.parseFloat(tblSLAList.getValueAt(selectedRow, 4).toString()) * 60;
            slaJSON.put("name", tblSLAList.getValueAt(selectedRow, 0));
            slaJSON.put("promql", tblSLAList.getValueAt(selectedRow, 1));
            slaJSON.put("min", tblSLAList.getValueAt(selectedRow, 2));
            slaJSON.put("max", tblSLAList.getValueAt(selectedRow, 3));
            slaJSON.put("seasonality", seasonality);
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + token);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            HttpEntity<String> messageBody = new HttpEntity<>(slaJSON.toString(), headers);
            try {
                ResponseEntity<String> response = restTemplate.exchange(urlSlamanager + "update", HttpMethod.PUT, messageBody, String.class);
                JOptionPane.showMessageDialog(null, response.getBody(), "Success", JOptionPane.INFORMATION_MESSAGE);
                btnRefreshActionPerformed(evt);
            } catch (HttpClientErrorException ex) {
                JOptionPane.showMessageDialog(null, ex.getResponseBodyAsString(), "Warning", JOptionPane.WARNING_MESSAGE);
                btnRefreshActionPerformed(evt);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "No response.", "Warning", JOptionPane.WARNING_MESSAGE);
                btnRefreshActionPerformed(evt);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No row selected.");
        }
    }//GEN-LAST:event_btnUpdateActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblSLAList;
    // End of variables declaration//GEN-END:variables
}
