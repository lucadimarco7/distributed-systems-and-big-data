# Progetto Applicazione "Traffic Event Notifier"
###### _Autori: Luca Dimarco - Davide Urboni_
###### _Anno di realizzazione: 2024_
## Sommario
- [Abstract](#abstract)
- [Note di Avvio](#note-di-avvio)
- [Note Aggiuntive](#note-aggiuntive)

## Abstract
Applicazione realizzata mediante un sistema distribuito con un’architettura basata su **microservizi** che permette agli utenti di ricevere degli aggiornamenti sul traffico relativi alle tratte di autobus in determinate fasce orarie.
Gli utenti possono registrarsi al servizio e impostare le proprie preferenze.
Nello specifico, l’utente può scegliere la tratta di interesse, la soglia di ritardo previsto e una soglia di tempo oltre la quale ricevere una notifica.
Inoltre, il sistema permette all’amministratore di monitorare le prestazioni mediante il controllo di metriche esposte dai vari microservizi.

Per lo sviluppo si è deciso di usare **Spring Boot** per la realizzazione della maggior parte dei microservizi, perché ne facilita la scrittura fornendo un set di librerie e strumenti preconfigurati. Per permettere un maggiore disaccoppiamento tra consumer e producer, è stato utilizzato **Kafka** in combinazione con **Zookeper**. Per quanto riguarda la persistenza dei dati sono stati utilizzati due database **MySQL** gestiti da un unico microservizio. I diversi microservizi vengono deployati come **Docker container**. 
Inoltre, è stata definita una configurazione che permette di deployare i microservizi mediante **Kubernetes**, orchestratore di container che permette una maggiore scalabilità, semplicità di gestione e maggiore visibilità.
I vari microservizi espongono delle **REST API** con le quali potersi interfacciare con l’utente o tra di loro. Infine, viene utilizzato **Prometheus** per la raccolta e il monitoraggio delle metriche. Il server Prometheus raccoglie le metriche effettuando uno scraping delle metriche esposte dai vari exporter.
In aggiunta, è stato creato anche un **client**, per facilitare l’utilizzo dell’applicazione attraverso l’interazione con un’interfaccia grafica.

## Note di Avvio
Prerequisiti per il build, il deploy e l’utilizzo dell’applicazione:

- **Docker**
- **Kubernetes**

Si consiglia l’utilizzo di **Docker Desktop** e attivare l’opzione “**Enable Kubernetes**” nelle impostazioni di quest’ultimo.
Per eseguire il build delle immagini dei microservizi creati basta eseguire:
```
docker-compose build
```
da riga di comando, nella directory principale del progetto.
Successivamente, se si vogliono avviare i container su docker, basta eseguire:
```
docker-compose up
```
Alla prima esecuzione del comando verrà eseguito anche il pull delle immagini esistenti su docker-hub.
Si può eseguire il tutto anche in un unico passaggio con il comando:
```
docker-compose up –build
```
Per un’esecuzione non verbose dei container si può aggiungere `-d` al comando.
Se si vuole utilizzare Kubernetes, invece, dopo aver eseguito docker-compose build bisogna spostarsi nella sottocartella k8s e sempre da riga di comando eseguire:
```
kubectl apply -R -f .
```
Per interrompere l’esecuzione si può eseguire il comando opposto:
```
kubectl delete -R -f .
```
Infine, per l’utilizzo dell’applicazione si consiglia l’uso del **client**. Se i container sono stati avviati tramite `docker-compose`, avviare il file `client.bat` all’interno della cartella **Client**. Altrimenti, se i container sono stati avviati tramite `kubectl`, avviare il file `client.bat` all’interno della cartella **Client-K8s**.

Per ulteriori delucidazioni sul progetto, consultare il file `Relazione.pdf` all'interno della repository.

## Note Aggiuntive
Se si volesse personalizzare l'applicazione si può modificare il file .env per:
- Aggiungere fasce orarie modificando `TIME_SLOT`.
- Aggiungere tratte modificando `ROAD_SECTIONS`.
- Modificare la frequenza di produce del producer modificando `SENDING_TIME`.
- Cambiare i databases e gli indirizzi modificando il resto delle variabili.
Tutte le chiavi segrete utilizzate, vengono passate come variabili d'ambiente. A scopo esemplificativo, sono state definite nei `Dockerfile`, si consiglia, invece, l'utilizzo di di servizi di Key Management (KMS).
    - In `ENV KEY_API` del `Dockerfile` del microservizio Producer mettere la chiave API del servizio routing di TomTom. 
    - In `ENV KEY` del `Dockerfile` dei microservizi Consumer e UserManager mettere la stessa chiave che verrà codificata in `HmacSHA512` per firmare il token.
    - In `ENV EMAIL` e `ENV PASSWORD` del `Dockerfile` del microservizio Notifier mettere le credenziali di GMail.
