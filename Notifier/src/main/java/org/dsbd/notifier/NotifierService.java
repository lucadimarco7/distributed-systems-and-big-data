package org.dsbd.notifier;

import jakarta.annotation.PostConstruct;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class NotifierService {
    private static Session session;
    private static final String email = System.getenv("EMAIL");
    private static final String password = System.getenv("PASSWORD");

    @PostConstruct
    private static void getSession() {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");

        session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(email, password);
            }
        });
    }

    public ConsumerMessage DeserializeMessage(String message){
        JSONObject jsonResponse = new JSONObject(message);
        JSONArray emails = jsonResponse.getJSONArray("emails");
        String startCity = jsonResponse.getString("startCity");
        String endCity = jsonResponse.getString("endCity");
        int travelTimeInSeconds = jsonResponse.getInt("travelTimeInSeconds");
        int noTrafficTravelTimeInSeconds = jsonResponse.getInt("noTrafficTravelTimeInSeconds");
        String departureTime = jsonResponse.getString("departureTime");

        String roadSection = startCity + "-" + endCity;
        int delay = travelTimeInSeconds - noTrafficTravelTimeInSeconds;
        String timeSlot = departureTime.substring(11, 16);
        return new ConsumerMessage(roadSection, delay, timeSlot, emails);
    }

    public void SendMail(String email, ConsumerMessage consumerMessage) throws Exception {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("provamailjava10@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject("Notifica sul Traffico");
            message.setText("Il ritardo previsto per " + consumerMessage.roadSection()
                    + " dovuto al traffico, nella fascia oraria "
                    + consumerMessage.timeSlot() + ", è di "
                    + consumerMessage.delay() + "s");
            Transport.send(message);
    }

}
