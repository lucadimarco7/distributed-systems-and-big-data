package org.dsbd.notifier;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Counter;

import javax.mail.MessagingException;

@RestController
public class NotifierController {
    private final Counter messageReceived;
    private final Counter sendedEmails;
    private final NotifierService notifierService;

    public NotifierController(MeterRegistry registry) {
        this.messageReceived = Counter.builder("received.message.notifier")
                .description("Messages received by the notifier.")
                .register(registry);
        this.sendedEmails = Counter.builder("sended.emails.notifier")
                .description("E-mails sended by the notifier.")
                .register(registry);
        this.notifierService = new NotifierService();
    }

    @PostMapping("/notifier")
    public ResponseEntity<String> notifier(@RequestBody String message) {
        messageReceived.increment();
        System.out.println("Message from Consumer: " + message);
        ConsumerMessage consumerMessage = notifierService.DeserializeMessage(message);
        for (var email : consumerMessage.emails()) {
            try {
                notifierService.SendMail(email.toString(), consumerMessage);
                sendedEmails.increment();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
        return ResponseEntity.ok("E-mail sent successfully.");
    }
}
