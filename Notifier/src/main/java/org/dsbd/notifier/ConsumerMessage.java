package org.dsbd.notifier;

import org.json.JSONArray;

public record ConsumerMessage(String roadSection, int delay, String timeSlot, JSONArray emails) {
}
