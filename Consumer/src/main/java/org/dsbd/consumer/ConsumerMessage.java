package org.dsbd.consumer;

public record ConsumerMessage(String timeSlot, String roadSection, int delay, int differenceTime) {
}
