package org.dsbd.consumer;

import com.google.gson.Gson;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import io.jsonwebtoken.Jwts;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;

@Service
public class ConsumerService {
    private final static String key = System.getenv("KEY");
    private static final SecretKey SECRET_KEY_CONSUMER = generateKey();
    private static final long EXPIRATION_TIME = 30000;
    private String token = generateToken();

    private final Counter emailsCounter;
    private final Counter messagesNotSendedCounter;
    private final Timer responseTimer;

    private final RestTemplate restTemplate;
    private final String urlUsermanager = "http://" + System.getenv("ADDRESS_USERMANAGER") + ":" + System.getenv("PORT_USERMANAGER")+ "/users/emails";
    private final String urlNotifier = "http://" + System.getenv("ADDRESS_NOTIFIER") + ":" + System.getenv("PORT_NOTIFIER")+ "/notifier";

    public ConsumerService(MeterRegistry registry) {
        this.restTemplate = new RestTemplate();
        this.emailsCounter = Counter.builder("emails.selected.consumer")
                .description("Emails selected by the consumer based on preferences.")
                .register(registry);
        this.messagesNotSendedCounter = Counter.builder("messages.not.sended.consumer")
                .description("Messages not sent by the consumer because no user has a preference.")
                .register(registry);
        this.responseTimer = Timer.builder("response.time.consumer")
                .description("Response time from the consummation of the message to the sending of the notification to the user.")
                .register(registry);
    }

    public void consumeFallback(String message, Exception e) {
        System.out.println("All retries failed.");
    }
    @Retry(name = "consumeRetry", fallbackMethod = "consumeFallback")
    @CircuitBreaker(name = "consumeCB")
    @KafkaListener(id = "${spring.kafka.consumer.group-id}", topics = "${topic}", containerFactory = "kafkaListenerContainerFactory")
    public void consume(String message) {
        responseTimer.record(()->{
            System.out.println("Message consumed from topic: " + message);
            ConsumerMessage consumerMessage;
            try {
                consumerMessage = DeserializerMessage(message);
            } catch (Exception ex){
                System.out.println(ex.getMessage());
                return;
            }
            Gson gson = new Gson();
            String consumerMessageJSON = gson.toJson(consumerMessage);
            ResponseEntity<String> response = sendToUsermanager(consumerMessageJSON);
            if (response.getStatusCode().equals(HttpStatusCode.valueOf(204))){
                System.out.println("Response body empty. No e-mail matching the preference.");
                messagesNotSendedCounter.increment();
                return;
            }
            if (response.getBody() == null){
                System.out.println("Response body empty. No e-mail matching the preference.");
                messagesNotSendedCounter.increment();
                return;
            }
            JSONObject filteredMessage = new JSONObject(message);
            JSONArray emails = new JSONArray(response.getBody());
            filteredMessage.put("emails", emails);
            for(int i=0 ; i < emails.length(); i++)
                emailsCounter.increment();
            System.out.println("messageBody: " + filteredMessage);
            HttpEntity<String> messageBody = new HttpEntity<>(filteredMessage.toString());
            try{
                response = restTemplate.postForEntity(urlNotifier, messageBody, String.class);
                System.out.println(response);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });
    }


    private ResponseEntity<String> sendToUsermanager(String consumerMessage) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + token);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            HttpEntity<String> entity = new HttpEntity<>(consumerMessage, headers);
            return restTemplate.postForEntity(urlUsermanager, entity, String.class);
        } catch (HttpClientErrorException.BadRequest ex) {
            token = generateToken();
            System.out.println("Token refreshed.");
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Token expired.");
        } catch (ResourceAccessException ex) {
            try {
                System.out.println("No response: next attempt in 5 seconds.");
                Thread.sleep(2000);
                throw new HttpClientErrorException(HttpStatus.SERVICE_UNAVAILABLE, "Service UserManager not available.");
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
                throw new HttpClientErrorException(HttpStatus.TOO_EARLY);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        }
    }

    public ConsumerMessage DeserializerMessage(String message) {
        JSONObject jsonResponse = new JSONObject(message);
        String startCity = jsonResponse.getString("startCity");
        String endCity = jsonResponse.getString("endCity");
        int travelTimeInSeconds = jsonResponse.getInt("travelTimeInSeconds");
        int noTrafficTravelTimeInSeconds = jsonResponse.getInt("noTrafficTravelTimeInSeconds");
        String departureTime = jsonResponse.getString("departureTime");
        int differenceTime = jsonResponse.getInt("differenceTime");

        String roadSection = startCity + "-" + endCity;
        int delay = travelTimeInSeconds - noTrafficTravelTimeInSeconds;

        String timeSlotString = departureTime.substring(11, 16);

        return new ConsumerMessage(timeSlotString, roadSection, delay, differenceTime);
    }

    private static SecretKeySpec generateKey(){
        byte[] keyBytes = Base64.getDecoder().decode(key);
        return new SecretKeySpec(keyBytes, "HmacSHA512");
    }

    public String generateToken() {
        return Jwts.builder()
                .claim("sender", "consumer")
                .expiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SECRET_KEY_CONSUMER)
                .compact();
    }
}
