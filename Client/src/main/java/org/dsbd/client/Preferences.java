package org.dsbd.client;

import java.util.Collections;
import javax.swing.JOptionPane;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class Preferences extends javax.swing.JPanel {
    private String token;
    private String urlUsermanager = "http://localhost:8080/users/";
    private final RestTemplate restTemplate = new RestTemplate();
    
    public Preferences(String token) {
        this.token = token;
        initComponents();
        myInitComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        lblRoadSection = new javax.swing.JLabel();
        lblTimeSlot = new javax.swing.JLabel();
        lblDelayThreshold = new javax.swing.JLabel();
        lblTimeThreshold = new javax.swing.JLabel();
        btnInsert = new javax.swing.JButton();
        cbTopics = new javax.swing.JComboBox<>();
        cbTimeSlots = new javax.swing.JComboBox<>();
        btnRefresh = new javax.swing.JButton();
        sDelay = new javax.swing.JSlider();
        spnDelay = new javax.swing.JSpinner();
        sTimeThreshold = new javax.swing.JSlider();
        spnTimeThreshold = new javax.swing.JSpinner();

        jLabel1.setText("jLabel1");

        jLabel4.setText("jLabel4");

        jLabel7.setText("jLabel7");

        jLabel8.setText("jLabel8");

        jTextField1.setText("jTextField1");

        jTextField2.setText("jTextField2");

        setPreferredSize(new java.awt.Dimension(460, 290));

        lblRoadSection.setText("Road Section");

        lblTimeSlot.setText("Time Slot");

        lblDelayThreshold.setText("Delay Threshold (min.)");

        lblTimeThreshold.setText("Time Threshold (min.)");

        btnInsert.setText("Insert");
        btnInsert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInsertActionPerformed(evt);
            }
        });

        btnRefresh.setText("Refresh Road Sections List");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        sDelay.setMaximum(1440);
        sDelay.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sDelayStateChanged(evt);
            }
        });

        spnDelay.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1440.0f), Float.valueOf(1.0f)));

        sTimeThreshold.setMaximum(1440);
        sTimeThreshold.setMinimum(15);
        sTimeThreshold.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sTimeThresholdStateChanged(evt);
            }
        });

        spnTimeThreshold.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(15.0f), Float.valueOf(15.0f), Float.valueOf(1440.0f), Float.valueOf(1.0f)));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDelayThreshold)
                    .addComponent(lblTimeSlot)
                    .addComponent(lblRoadSection)
                    .addComponent(lblTimeThreshold))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(sTimeThreshold, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spnTimeThreshold, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cbTopics, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbTimeSlots, 0, 396, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnInsert))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(sDelay, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spnDelay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(30, 30, 30))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblRoadSection)
                    .addComponent(cbTopics, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTimeSlot)
                    .addComponent(cbTimeSlots, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDelayThreshold)
                    .addComponent(sDelay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spnDelay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTimeThreshold)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(sTimeThreshold, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(spnTimeThreshold, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnInsert)
                    .addComponent(btnRefresh))
                .addContainerGap(77, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void myInitComponents(){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + token);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        try {
            ResponseEntity<String> response = restTemplate.exchange(urlUsermanager + "roadSections", HttpMethod.GET, entity, String.class);
            JSONArray responseArray = new JSONArray(response.getBody());
            cbTopics.removeAllItems();
            for(Object topicName : responseArray){
                cbTopics.addItem(topicName.toString());
            }
            response = restTemplate.exchange(urlUsermanager + "timeSlots", HttpMethod.GET, entity, String.class);
            responseArray = new JSONArray(response.getBody());
            System.out.println(responseArray);
            cbTimeSlots.removeAllItems();
            for(Object timeSlots : responseArray){
                cbTimeSlots.addItem(timeSlots.toString());
            }
        } catch (HttpClientErrorException ex) {
            JOptionPane.showMessageDialog(null, ex.getResponseBodyAsString(), "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }
    
    private void btnInsertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInsertActionPerformed
        JSONObject preferencesJSON = new JSONObject();
        float delayThreshold = Float.parseFloat(spnDelay.getValue().toString()) * 60;
        float timeThreshold = Float.parseFloat(spnTimeThreshold.getValue().toString()) * 60;
        preferencesJSON.put("roadSection", cbTopics.getSelectedItem());
        preferencesJSON.put("timeSlot", cbTimeSlots.getSelectedItem());
        preferencesJSON.put("delayThreshold", delayThreshold);
        preferencesJSON.put("timeThreshold", timeThreshold);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + token);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> messageBody = new HttpEntity<>(preferencesJSON.toString(), headers);
        try {
            ResponseEntity<String> response = restTemplate.postForEntity(urlUsermanager + "preferences", messageBody, String.class);
            JOptionPane.showMessageDialog(null, response.getBody(), "Success", JOptionPane.INFORMATION_MESSAGE);
        } catch (HttpClientErrorException ex) {
            JOptionPane.showMessageDialog(null, ex.getResponseBodyAsString(), "Warning", JOptionPane.WARNING_MESSAGE);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "No response.", "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnInsertActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        myInitComponents();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void sDelayStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sDelayStateChanged
        spnDelay.setValue(sDelay.getValue());
    }//GEN-LAST:event_sDelayStateChanged

    private void sTimeThresholdStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sTimeThresholdStateChanged
        spnTimeThreshold.setValue(sTimeThreshold.getValue());
    }//GEN-LAST:event_sTimeThresholdStateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnInsert;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JComboBox<String> cbTimeSlots;
    private javax.swing.JComboBox<String> cbTopics;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JLabel lblDelayThreshold;
    private javax.swing.JLabel lblRoadSection;
    private javax.swing.JLabel lblTimeSlot;
    private javax.swing.JLabel lblTimeThreshold;
    private javax.swing.JSlider sDelay;
    private javax.swing.JSlider sTimeThreshold;
    private javax.swing.JSpinner spnDelay;
    private javax.swing.JSpinner spnTimeThreshold;
    // End of variables declaration//GEN-END:variables
}
