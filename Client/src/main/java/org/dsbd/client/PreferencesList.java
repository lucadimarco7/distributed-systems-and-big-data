package org.dsbd.client;

import java.util.Collections;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class PreferencesList extends javax.swing.JPanel {
    private String token;
    private String urlUsermanager = "http://localhost:8080/users/";
    private final RestTemplate restTemplate = new RestTemplate();

    public PreferencesList(String token) {
        this.token = token;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnRefresh = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblPreferencesList = new javax.swing.JTable();
        btnDelete = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();

        btnRefresh.setText("Refresh");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        tblPreferencesList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Road Section", "Time Slot", "Delay Threshold (min.)", "Time Threshold (min.)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Long.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return column != 0;
            }
        });
        tblPreferencesList.setRowHeight(25);
        tblPreferencesList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(tblPreferencesList);

        btnDelete.setBackground(new java.awt.Color(255, 102, 153));
        btnDelete.setForeground(java.awt.SystemColor.activeCaptionText);
        btnDelete.setText("Delete Selected");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnUpdate.setText("Update Selected");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUpdate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelete))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 497, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 353, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRefresh)
                    .addComponent(btnDelete)
                    .addComponent(btnUpdate))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + token);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        DefaultTableModel model = (DefaultTableModel) tblPreferencesList.getModel();
        try {
            ResponseEntity<String> response = restTemplate.exchange(urlUsermanager + "preferencesList", HttpMethod.GET, entity, String.class);
            JSONObject responseJSON = new JSONObject(response.getBody());
            model.setRowCount(0);
            for(String id : responseJSON.keySet()){
                String preference = responseJSON.getString(id);
                JSONObject jsonObject = new JSONObject(preference);
                float delayThreshold = jsonObject.getFloat("delayThreshold") / 60;
                float timeThreshold = jsonObject.getFloat("timeThreshold") / 60;
                Object[] row = {Long.parseLong(id), jsonObject.getString("roadSection"), jsonObject.getString("timeSlot"), delayThreshold, timeThreshold};
                model.addRow(row);
            }
        } catch (HttpClientErrorException ex) {
            model.setRowCount(0);
            JOptionPane.showMessageDialog(null, ex.getResponseBodyAsString(), "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedRow = tblPreferencesList.getSelectedRow();
        if (selectedRow != -1) {
            JSONObject deleteJSON = new JSONObject();
            deleteJSON.put("id", tblPreferencesList.getValueAt(selectedRow, 0));
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + token);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            HttpEntity<String> messageBody = new HttpEntity<>(deleteJSON.toString(), headers);
            try{
                ResponseEntity<String> response = restTemplate.exchange(urlUsermanager + "deletePreference", HttpMethod.DELETE, messageBody, String.class);
                JOptionPane.showMessageDialog(null, response.getBody(), "Success", JOptionPane.INFORMATION_MESSAGE);
                btnRefreshActionPerformed(evt);
            } catch (HttpClientErrorException ex) {
                JOptionPane.showMessageDialog(null, ex.getResponseBodyAsString(), "Warning", JOptionPane.WARNING_MESSAGE);
                btnRefreshActionPerformed(evt);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "No response.", "Warning", JOptionPane.WARNING_MESSAGE);
                btnRefreshActionPerformed(evt);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No row selected.");
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        int selectedRow = tblPreferencesList.getSelectedRow();
        if (selectedRow != -1) {
            JSONObject preferenceJSON = new JSONObject();
            float delayThreshold = Float.parseFloat(tblPreferencesList.getValueAt(selectedRow, 3).toString()) * 60;
            float timeThreshold = Float.parseFloat(tblPreferencesList.getValueAt(selectedRow, 4).toString()) * 60;
            preferenceJSON.put("roadSection", tblPreferencesList.getValueAt(selectedRow, 1));
            preferenceJSON.put("timeSlot", tblPreferencesList.getValueAt(selectedRow, 2));
            preferenceJSON.put("delayThreshold", delayThreshold);
            preferenceJSON.put("timeThreshold", timeThreshold);
            JSONObject dataJSON = new JSONObject();
            dataJSON.put(tblPreferencesList.getValueAt(selectedRow, 0).toString(), preferenceJSON);
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + token);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            HttpEntity<String> messageBody = new HttpEntity<>(dataJSON.toString(), headers);
            try {
                ResponseEntity<String> response = restTemplate.exchange(urlUsermanager + "updatePreference", HttpMethod.PUT, messageBody, String.class);
                JOptionPane.showMessageDialog(null, response.getBody(), "Success", JOptionPane.INFORMATION_MESSAGE);
                btnRefreshActionPerformed(evt);
            } catch (HttpClientErrorException ex) {
                JOptionPane.showMessageDialog(null, ex.getResponseBodyAsString(), "Warning", JOptionPane.WARNING_MESSAGE);
                btnRefreshActionPerformed(evt);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "No response.", "Warning", JOptionPane.WARNING_MESSAGE);
                btnRefreshActionPerformed(evt);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No row selected.");
        }
    }//GEN-LAST:event_btnUpdateActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblPreferencesList;
    // End of variables declaration//GEN-END:variables
}
