from prometheus_api_client import PrometheusConnect
from datetime import datetime, timedelta
from datetime import timedelta
import pytz
import io
import os
import base64
import matplotlib.pyplot as plt
import pandas as pd
from pmdarima import auto_arima
import statsmodels.api as sm
from sklearn.linear_model import LinearRegression
from flask import Flask, request, jsonify, make_response

app = Flask(__name__)
if "ADDRESS_PROMETHEUS" or "PORT_PROMETHEUS" in os.environ: 
    ADDRESS_PROMETHEUS = os.environ['ADDRESS_PROMETHEUS']
    PORT_PROMETHEUS = os.environ['PORT_PROMETHEUS']
else:
    print("Enviroment variables not found.")
prom = PrometheusConnect(url="http://" + ADDRESS_PROMETHEUS + ":"+ PORT_PROMETHEUS, disable_ssl=True)

def get_time_series(query, start_time, end_time, step="10s"):
    response = prom.custom_query_range(
        query=query,
        start_time=start_time,
        end_time=end_time,
        step=step
    )
    return response

def convert_utc_to_local(utc_time, local_timezone):
    return utc_time.replace(tzinfo=pytz.utc).astimezone(pytz.timezone(local_timezone))

@app.route('/linearRegression', methods=['POST'])
def linearRegression():
    data = request.get_json()
    query = str(data.get('query'))
    min = float(data.get('min'))
    max = float(data.get('max'))
    minutes = int(data.get('minutes'))

    now = datetime.now()
    one_hour_ago = now - timedelta(hours=1)

    data = get_time_series(query, one_hour_ago, now)

    timestamps = [entry[0] for entry in data[0]['values']]
    values = [float(entry[1]) for entry in data[0]['values']]
    df = pd.DataFrame({'Timestamp': timestamps, 'Values': values})

    df['Timestamp'] = pd.to_datetime(df['Timestamp'], unit='s')
    df['Timestamp_numeric'] = df['Timestamp'].astype('int64')

    model = LinearRegression()
    
    prob_violation = 0

    plt.figure(figsize=(12, 6))
    plt.title('Time Series with Regression and Future Prediction')
    plt.xlabel('Timestamps')
    plt.ylabel('Values')
    
    try:
        model.fit(df['Timestamp_numeric'].values.reshape(-1, 1), df['Values'])
        predictions = model.predict(df['Timestamp_numeric'].values.reshape(-1, 1))

        future_timestamps = pd.date_range(df['Timestamp'].max(), df['Timestamp'].max() + timedelta(minutes=minutes), freq='S')
        future_timestamps_numeric = future_timestamps.astype('int64')
        future_predictions = model.predict(future_timestamps_numeric.values.reshape(-1, 1))

        violation = 0
        for pred in future_predictions:
            if pred < min or pred > max:
                violation = violation + 1
        prob_violation = round(violation / len(future_predictions) * 100, 2)

        plt.plot(df['Timestamp'], df['Values'], label='Time Series')
        plt.plot(df['Timestamp'], predictions, label='Regression', linestyle='dashed', color='green')
        plt.plot(future_timestamps, future_predictions, label='Future Prediction', linestyle='dashed', color='orange')   
        plt.legend()

    except Exception as e:
        error_message = str(e)
        response = make_response(jsonify({'error': error_message}), 500)
        return response
      
    img_buf = io.BytesIO()
    plt.savefig(img_buf, format='png')
    img_buf.seek(0)
    img_base64 = base64.b64encode(img_buf.read()).decode('utf-8')
    plt.close()

    return jsonify({'image': img_base64, 'prob_violation': prob_violation})

@app.route('/sarimax', methods=['POST'])
def sarimax():
    data = request.get_json()
    query = str(data.get('query'))
    min = float(data.get('min'))
    max = float(data.get('max'))
    seasonality = float(data.get('seasonality'))
    minutes = int(data.get('minutes'))

    resample_num = 60

    now_utc = datetime.utcnow()
    one_hour_ago_utc = now_utc - timedelta(hours=1)
    local_timezone = 'Europe/Rome'
    now_local = convert_utc_to_local(now_utc, local_timezone)
    one_hour_ago_local = convert_utc_to_local(one_hour_ago_utc, local_timezone)

    data = get_time_series(query, one_hour_ago_local, now_local)

    timestamps = [entry[0] for entry in data[0]['values']]
    values = [float(entry[1]) for entry in data[0]['values']]
    df = pd.DataFrame({'Timestamp': timestamps, 'Values': values})

    df['Timestamp'] = pd.to_datetime(df['Timestamp'], unit='s')
    df.set_index('Timestamp', inplace=True)
    df_resampled = df.resample(str(resample_num) + 's').mean()
    df_resampled.interpolate()

    m = round(seasonality / resample_num)
    
    try:
        stepwise_fit = auto_arima(df_resampled['Values'], seasonal=True, m=m)
        p, d, q = stepwise_fit.order
        P, D, Q, m = stepwise_fit.seasonal_order
        print(stepwise_fit.summary())

        model = sm.tsa.SARIMAX(df_resampled['Values'], order=(p, d, q), seasonal_order=(P, D, Q, m))
        
        
        prob_violation = 0

        plt.figure(figsize=(12, 6))
        plt.title('Time Series and Future Prediction with model SARIMAX')
        plt.xlabel('Timestamps')
        plt.ylabel('Values')
    
        model_fit = model.fit()

        num_steps = round(minutes * 60 / resample_num)
        forecast = model_fit.get_forecast(steps=num_steps)
        forecast_dates_local = [convert_utc_to_local(date, local_timezone) for date in pd.date_range(start=now_utc, periods=num_steps, freq= str(resample_num) + 's')]
    
        violation = 0
        for pred in forecast.predicted_mean:
            if pred < min or pred > max:
                violation = violation + 1
        prob_violation = round(violation / len(forecast.predicted_mean) * 100, 2)

        plt.plot(df_resampled.index, df_resampled['Values'], label='Time Series')
        plt.plot(forecast_dates_local, forecast.predicted_mean, label='Forecast', linestyle='dashed', color='orange')   
        plt.legend()

    except Exception as e:
        error_message = str(e)
        response = make_response(jsonify({'error': error_message}), 500)
        return response
      
    img_buf = io.BytesIO()
    plt.savefig(img_buf, format='png')
    img_buf.seek(0)
    img_base64 = base64.b64encode(img_buf.read()).decode('utf-8')
    plt.close()

    return jsonify({'image': img_base64, 'prob_violation': prob_violation})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)