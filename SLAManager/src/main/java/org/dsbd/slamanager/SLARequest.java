package org.dsbd.slamanager;

public record SLARequest(String name, String promql, String description, float min, float max, float seasonality) {
}
