package org.dsbd.slamanager;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = "name")
})
public class Metric {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany(mappedBy = "metric")
    private Set<SLA> slas;
    private String promQL;
    private String description;
    private float seasonality;

    public Metric() {
    }

    public Metric(String promQL, String description, float seasonality) {
        this.promQL = promQL;
        this.description = description;
        this.seasonality = seasonality;
    }

    public String getPromQL() {
        return promQL;
    }

    public String getDescription() {
        return description;
    }

    public float getSeasonality() {
        return seasonality;
    }

    public void setSeasonality(float seasonality) {
        this.seasonality = seasonality;
    }
}
