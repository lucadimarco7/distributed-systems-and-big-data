package org.dsbd.slamanager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

@RestController
public class SLAController {
    private final SLAService slaService;
    private final AuthService authService;

    public SLAController(SLAService slaService, AuthService authService) {
        this.slaService = slaService;
        this.authService = authService;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleCustomExceptions(Exception ex) {
        if (ex instanceof HttpClientErrorException) {
            return ResponseEntity.status(((HttpClientErrorException) ex).getStatusCode()).body(ex.getMessage());
        }
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @PostMapping("/login")
    public ResponseEntity<String> adminLogin(@RequestBody LoginRequest loginRequest) {
        Long id = authService.authenticateAdmin(loginRequest);
        String token = authService.generateToken(id);
        return ResponseEntity.ok("Successfully logged in. Token: " + token);
    }

    @PutMapping("/updateAdmin")
    public ResponseEntity<String> adminUpdate(@RequestHeader(name = "Authorization") String token, @RequestBody AdminUpdateRequest adminUpdateRequest) {
        Long id = authService.authorizeAdmin(token);
        authService.updateAdmin(id, adminUpdateRequest);
        return ResponseEntity.ok("Update successfully completed.");
    }

    @PostMapping("/create")
    public ResponseEntity<String> createSLA(@RequestHeader(name = "Authorization") String token, @RequestBody SLARequest slaRequest) {
        authService.authorizeAdmin(token);
        slaService.create(slaRequest);
        return ResponseEntity.ok("SLA created successfully.");
    }

    @PutMapping("/update")
    public ResponseEntity<String> updateSLA(@RequestHeader(name = "Authorization") String token, @RequestBody SLARequest slaRequest) {
        authService.authorizeAdmin(token);
        slaService.update(slaRequest);
        return ResponseEntity.ok("SLA successfully updated.");
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> deleteSLA(@RequestHeader(name = "Authorization") String token, @RequestBody String name) {
        authService.authorizeAdmin(token);
        JSONObject requestObject = new JSONObject(name);
        slaService.delete(requestObject.get("name").toString());
        return ResponseEntity.ok("SLA successfully deleted.");
    }

    @GetMapping("/customMetricList")
    public ResponseEntity<String> customMetric(@RequestHeader(name = "Authorization") String token) {
        authService.authorizeAdmin(token);
        JSONArray customMetricList = new JSONArray(slaService.getCustomMetricList());
        return ResponseEntity.ok(customMetricList.toString());
    }

    @GetMapping("/status/{name}")
    public ResponseEntity<String> statusSLA(@RequestHeader(name = "Authorization") String token, @PathVariable("name") String name) {
        authService.authorizeAdmin(token);
        JSONObject status = slaService.getSLAStatus(name);
        return ResponseEntity.ok(status.toString());
    }

    @GetMapping("/violations/{hours}")
    public ResponseEntity<String> violationsSLA(@RequestHeader(name = "Authorization") String token, @PathVariable("hours") int hours) {
        authService.authorizeAdmin(token);
        JSONObject violations = slaService.getViolationsCount(hours);
        return ResponseEntity.ok(violations.toString());
    }

    @PostMapping("/prediction")
    public ResponseEntity<String> prediction(@RequestHeader(name = "Authorization") String token, @RequestBody String request){
        authService.authorizeAdmin(token);
        JSONObject data = new JSONObject(request);
        return slaService.prediction(data);
    }

    @GetMapping("/metricsList")
    public ResponseEntity<String> getMetrics(@RequestHeader(name = "Authorization") String token) {
        authService.authorizeAdmin(token);
        JSONArray response = slaService.getMetricsList();
        return ResponseEntity.ok(response.toString());
    }

    @GetMapping("/slaList")
    public ResponseEntity<String> getSLA(@RequestHeader(name = "Authorization") String token) {
        authService.authorizeAdmin(token);
        JSONArray response = slaService.getSLAList();
        return ResponseEntity.ok(response.toString());
    }
}
