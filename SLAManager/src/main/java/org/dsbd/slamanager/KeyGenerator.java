package org.dsbd.slamanager;

import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;

public class KeyGenerator {

    public static SecretKeySpec generateRandomKey() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] keyBytes = new byte[64]; // 512 bit = 64 byte
        secureRandom.nextBytes(keyBytes);
        return new SecretKeySpec(keyBytes, "HmacSHA512");
    }

}

