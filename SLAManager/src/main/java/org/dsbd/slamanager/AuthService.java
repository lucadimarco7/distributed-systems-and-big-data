package org.dsbd.slamanager;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.Optional;

@Service
public class AuthService {

    private final IAdminRepository adminRepository;

    private static final SecretKey SECRET_KEY = KeyGenerator.generateRandomKey();
    private static final long EXPIRATION_TIME = 3600000; //1 ora in millisecondi

    private final PasswordEncoder passwordEncoder;

    @Lazy
    public AuthService(IAdminRepository adminRepository, PasswordEncoder passwordEncoder) {
        this.adminRepository = adminRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    public void encodePassword(Admin admin, String password) {
        admin.setPassword(passwordEncoder.encode(password));
    }


    public Long authenticateAdmin(LoginRequest loginRequest) {
        Optional<Admin> adminOptional = adminRepository.findByUsername(loginRequest.username());
        Admin admin;
        if (adminOptional.isPresent()) {
            admin = adminOptional.get();
            if (!passwordEncoder.matches(loginRequest.password(), admin.getPassword()))
                throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Wrong password.");
        } else {
            throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Admin not found.");
        }
        return admin.getId();
    }

    public Long authorizeAdmin(String token) {
        if (token.startsWith("Bearer ")) {
            token = token.substring(7);
        }

        Jws<Claims> jws = Jwts.parser().verifyWith(SECRET_KEY).build().parseSignedClaims(token);
        Claims claims = jws.getPayload();

        Date expiration = claims.getExpiration();
        if (isTokenExpired(expiration))
            throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Token expired.");

        String tokenId = claims.get("id").toString();
        Long id = Long.parseLong(tokenId);
        if (!adminRepository.existsById(id))
            throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Not authorized.");

        return id;
    }

    public String generateToken(Long id) {
        return Jwts.builder()
                .claim("id", id)
                .expiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SECRET_KEY)
                .compact();
    }

    private boolean isTokenExpired(Date expiration) {
        return expiration.before(new Date());
    }

    public void updateAdmin(Long id, AdminUpdateRequest adminUpdateRequest) {
        if (adminUpdateRequest.newPassword().length() < 6)
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Invalid credentials.");
        Admin admin = adminRepository.findById(id).orElse(null);
        if (admin != null) {
            if (!passwordEncoder.matches(adminUpdateRequest.oldPassword(), admin.getPassword()))
                throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Wrong password.");
            admin.setUsername(adminUpdateRequest.username());
            admin.setPassword(passwordEncoder.encode(adminUpdateRequest.newPassword()));
            adminRepository.save(admin);
        } else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "User not found.");
        }
    }
}
