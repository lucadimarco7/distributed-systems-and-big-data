package org.dsbd.slamanager;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class AdminInitializer implements CommandLineRunner {

    private final IAdminRepository adminRepository;
    private final AuthService authService;

    public AdminInitializer(IAdminRepository adminRepository, AuthService authService) {
        this.adminRepository = adminRepository;
        this.authService = authService;
    }

    @Override
    public void run(String... args){
        if (adminRepository.count() == 0) {
            Admin admin = new Admin("admin", "admin");
            authService.encodePassword(admin, admin.getPassword());
            adminRepository.save(admin);
        } else {
            System.out.println("La tabella Admin non è vuota, nessuna operazione di inserimento necessaria.");
        }
    }
}


