package org.dsbd.slamanager;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ISLARepository extends JpaRepository<SLA, Long> {
    Optional<SLA> findByName(String name);
    void deleteByName(String name);
}
