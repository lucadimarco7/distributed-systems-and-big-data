package org.dsbd.slamanager;

public record LoginRequest(String username, String password) {
}
