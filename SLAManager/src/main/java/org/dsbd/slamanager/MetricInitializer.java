package org.dsbd.slamanager;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MetricInitializer implements CommandLineRunner {
    private final IMetricRepository promQLRepository;

    public MetricInitializer(IMetricRepository promQLRepository) {
        this.promQLRepository = promQLRepository;
    }

    @Override
    public void run(String... args) {
        if (!promQLRepository.findAll().isEmpty()) {
            return;
        }

        savePromQL("response_time_consumer_seconds_max",
                "Max response time from consuming the message to sending the notification to the user.");
        savePromQL("response_time_consumer_seconds_sum/response_time_consumer_seconds_count",
                "Average response time for measured events, from consuming the message to sending the notification to the user.");
        savePromQL("response_time_producer_seconds_max",
                "Max response time to produce all data by the producer.");
        savePromQL("response_time_producer_seconds_sum/response_time_producer_seconds_count",
                "Average response time to produce all data by the producer.");
        savePromQL("sum(increase(logins_total[24h]))",
                "Login number in the last 24 hours.");
        savePromQL("sum(increase(registrations_total[24h]))",
                "Register number in the last 24 hours.");
        savePromQL("sended_emails_notifier_total",
                "E-mails sended by the notifier.");

    }

    private void savePromQL(String promql, String description){
        Metric entity = new Metric(promql, description, (float) 900);
        promQLRepository.save(entity);
    }
}

