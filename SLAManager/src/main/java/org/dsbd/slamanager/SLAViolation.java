package org.dsbd.slamanager;

import jakarta.persistence.*;

import java.util.Date;
import java.util.Set;

@Entity
public class SLAViolation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date timestamp;
    @ManyToOne
    @JoinColumn(name = "sla_id")
    private SLA sla;

    public SLAViolation(){}
    public SLAViolation(Date timestamp, SLA sla) {
        this.timestamp = timestamp;
        this.sla = sla;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public SLA getSla() {
        return sla;
    }
}
