package org.dsbd.slamanager;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface ISLAViolationRepository extends JpaRepository<SLAViolation, Long> {
    List<SLAViolation> findByTimestampBetween(Date start, Date end);

    void deleteAllBySlaId(Long id);

}
