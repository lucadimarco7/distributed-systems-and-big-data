package org.dsbd.slamanager;

public record AdminUpdateRequest(String username, String oldPassword, String newPassword) {
}
