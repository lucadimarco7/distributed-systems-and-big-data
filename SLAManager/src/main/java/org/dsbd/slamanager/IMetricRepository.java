package org.dsbd.slamanager;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IMetricRepository extends JpaRepository<Metric, Long> {
    Optional<Metric> findMetricByPromQL(String query);
}
