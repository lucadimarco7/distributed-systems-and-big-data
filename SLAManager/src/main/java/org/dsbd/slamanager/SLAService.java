package org.dsbd.slamanager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
public class SLAService {
    private final ISLARepository slaRepository;
    private final IMetricRepository metricRepository;
    private final ISLAViolationRepository slaViolationRepository;
    private final RestTemplate restTemplate;
    private final String prometheusServer = "http://" + System.getenv("ADDRESS_PROMETHEUS") + ":" + System.getenv("PORT_PROMETHEUS");
    private final String predictionServer = "http://" + System.getenv("ADDRESS_PREDICTION") + ":" + System.getenv("PORT_PREDICTION");


    @Lazy
    public SLAService(ISLARepository slaRepository, IMetricRepository metricRepository, ISLAViolationRepository slaViolationRepository) {
        this.slaRepository = slaRepository;
        this.metricRepository = metricRepository;
        this.slaViolationRepository = slaViolationRepository;
        this.restTemplate = new RestTemplate();
    }

    public void create(SLARequest slaRequest) {
        verifyQuery(slaRequest.promql());
        verifySLA(slaRequest);
        Metric metric = metricRepository.findMetricByPromQL(slaRequest.promql()).orElse(null);
        if(metric == null) {
            metric = new Metric(slaRequest.promql(), slaRequest.description(), slaRequest.seasonality());
            metricRepository.save(metric);
        } else if (metric.getSeasonality() != slaRequest.seasonality()) {
            metric.setSeasonality(slaRequest.seasonality());
            metricRepository.save(metric);
        }
        SLA sla = new SLA(slaRequest.name(), metric, slaRequest.min(), slaRequest.max());
        slaRepository.save(sla);
    }

    private void verifyQuery(String promql) {
        URI queryUri = encodeQuery(promql);
        ResponseEntity<String> queryResponse = restTemplate.getForEntity(queryUri, String.class);
        System.out.println("Query Response: " + queryResponse.getBody());
        JSONObject queryResponseJson = new JSONObject(queryResponse.getBody());
        JSONObject data = queryResponseJson.getJSONObject("data");
        JSONArray result = data.getJSONArray("result");
        if(result.isEmpty())
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Query is not valid.");
    }

    private void verifySLA(SLARequest slaRequest) {
        if(slaRequest.min() < 0)
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "The minimum value is less than 0.");
        if(slaRequest.max() <= slaRequest.min())
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "The maximum value is less than or equal to the minimum value.");
        if(slaRequest.name().isEmpty())
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "The name is empty.");
        if(slaRequest.description().isEmpty())
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "The description is empty.");
        if(slaRequest.seasonality() < 0)
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "The seasonality is less than 0.");

        SLA sla = slaRepository.findByName(slaRequest.name()).orElse(null);
        if(sla != null)
            throw new HttpClientErrorException(HttpStatus.CONFLICT, "SLA already exist.");
    }

    public void update(SLARequest slaRequest) {
        if(slaRequest.min() < 0)
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "The minimum value is less than 0.");
        if(slaRequest.max() <= slaRequest.min())
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "The maximum value is less than or equal to the minimum value.");

        String slaName = slaRequest.name();
        SLA slaOptional = slaRepository.findByName(slaName).orElse(null);
        if (slaOptional != null) {
            verifyQuery(slaRequest.promql());
            Metric metric = metricRepository.findMetricByPromQL(slaRequest.promql()).orElse(null);
            if(metric == null) {
                metric = new Metric(slaRequest.promql(), slaRequest.description(), slaRequest.seasonality());
                metricRepository.save(metric);
            }
            slaOptional.setMetric(metric);
            slaOptional.setMin(slaRequest.min());
            slaOptional.setMax(slaRequest.max());
            slaRepository.save(slaOptional);
        } else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "SLA not found.");
        }
    }

    @Transactional
    public void delete(String name) {
        SLA slaOptional = slaRepository.findByName(name).orElse(null);
        if(slaOptional == null){
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "SLA not found.");
        }
        slaViolationRepository.deleteAllBySlaId(slaOptional.getId());
        slaRepository.deleteByName(name);
    }

    private URI encodeQuery(String query) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(prometheusServer + "/api/v1/query")
                .queryParam("query", query);
        return builder.build().encode().toUri();
    }

    public String getQuery(String name) {
        SLA sla = slaRepository.findByName(name).orElse(null);
        if(sla == null)
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "SLA " + name + " query not found.");
        return sla.getMetric().getPromQL();
    }

    public JSONArray getSLAList() {
        List<SLA> slaList = slaRepository.findAll();
        JSONArray jsonSlaList = new JSONArray();
        for(SLA sla : slaList){
            JSONObject slaObject = new JSONObject();
            slaObject.put("sla", sla.getName());
            slaObject.put("promql", sla.getMetric().getPromQL());
            slaObject.put("min", sla.getMin());
            slaObject.put("max", sla.getMax());
            slaObject.put("seasonality", sla.getMetric().getSeasonality());
            jsonSlaList.put(slaObject);
        }
        return jsonSlaList;
    }

    public JSONArray getCustomMetricList() {
        JSONArray customMetricList = new JSONArray();
        List<Metric> metricList = metricRepository.findAll();
        for (Metric metric : metricList) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("description", metric.getDescription());
            jsonObject.put("promQL", metric.getPromQL());
            jsonObject.put("seasonality", metric.getSeasonality());
            customMetricList.put(jsonObject);
        }
        return customMetricList;
    }

    private String getMetricValue(ResponseEntity<String> queryResponse) {
        System.out.println(queryResponse.getBody());
        JSONObject responseJSON = new JSONObject(queryResponse.getBody());
        System.out.println(responseJSON);
        JSONObject data = responseJSON.getJSONObject("data");
        JSONArray result = data.getJSONArray("result");
        System.out.println(result);
        JSONObject firstResult = new JSONObject(result.get(0).toString());
        JSONArray value = firstResult.getJSONArray("value");
        return value.get(1).toString();
    }

    public JSONArray getMetricsList() {
        String metricsListUrl = prometheusServer + "/api/v1/label/__name__/values";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.exchange(metricsListUrl, HttpMethod.GET, null, String.class);
        JSONObject jsonResponse = new JSONObject(response.getBody());
        JSONArray data = new JSONArray(jsonResponse.getJSONArray("data"));
        JSONArray metricsList = new JSONArray();
        for(Object metric : data){
            JSONObject metrics = new JSONObject();
            URI queryURI = encodeQuery(metric.toString());
            ResponseEntity<String> queryResponse = restTemplate.getForEntity(queryURI, String.class);
            String value = getMetricValue(queryResponse);
            metrics.put("metric", metric);
            metrics.put("value", value);
            metricsList.put(metrics);
        }
        return metricsList;
    }

    @Scheduled(fixedRate = 10000)
    public void setViolations() {
        List<SLA> slaList = slaRepository.findAll();
        for(SLA sla : slaList){
            try{
                JSONObject jsonData = getSLAStatus(sla.getName());
                boolean violationStatus = jsonData.getBoolean("violationStatus");
                if(violationStatus){
                    Date timestamp = new Date();
                    SLAViolation slaViolation = new SLAViolation(timestamp, sla);
                    slaViolationRepository.save(slaViolation);
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public JSONObject getSLAStatus(String slaName) {
        System.out.println(slaName);
        SLA slaOptional = slaRepository.findByName(slaName).orElse(null);
        if (slaOptional == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "SLA not found.");
        }
        String query = getQuery(slaOptional.getName());
        URI queryURI = encodeQuery(query);
        ResponseEntity<String> queryResponse = restTemplate.getForEntity(queryURI, String.class);
        String value = getMetricValue(queryResponse);
        float valueFloat = Float.parseFloat(value);
        boolean isViolated = !(valueFloat >= slaOptional.getMin() && valueFloat <= slaOptional.getMax());
        JSONObject jsonData = new JSONObject();
        jsonData.put("value", value);
        jsonData.put("violationStatus", isViolated);
        return jsonData;
    }

    public JSONObject getViolationsCount(int hours) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime result = now.minusHours(hours);
        Date nowDate = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
        Date resultDate = Date.from(result.atZone(ZoneId.systemDefault()).toInstant());
        List<SLAViolation> slaViolationList = slaViolationRepository.findByTimestampBetween(resultDate, nowDate);
        Map<String, Integer> violations = getViolationsMap(slaViolationList);
        return new JSONObject(violations);
    }

    private static Map<String, Integer> getViolationsMap(List<SLAViolation> slaViolationList) {
        Map<String, Integer> violations = new HashMap<>();
        for(SLAViolation slaViolation : slaViolationList){
            String name = slaViolation.getSla().getName();
            if(violations.containsKey(name)){
                violations.put(name, violations.get(name) + 1);
            } else {
                violations.put(name, 1);
            }
        }
        return violations;
    }

    public ResponseEntity<String> prediction(JSONObject data) {
        String sla = data.getString("sla");
        String query = getQuery(sla);
        SLA slaOptional = slaRepository.findByName(sla).orElse(null);
        if(slaOptional == null){
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "SLA not found.");
        }
        float seasonality = slaOptional.getMetric().getSeasonality();
        data.put("query", query);
        data.put("min", slaOptional.getMin());
        data.put("max", slaOptional.getMax());
        data.put("seasonality", seasonality);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<String> messageBody = new HttpEntity<>(data.toString(), headers);
        if (seasonality == 0)
            return restTemplate.postForEntity(predictionServer + "/linearRegression", messageBody, String.class);
        return restTemplate.postForEntity(predictionServer + "/sarimax", messageBody, String.class);
    }
}
