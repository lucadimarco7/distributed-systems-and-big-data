package org.dsbd.slamanager;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = "name")
})
public class SLA {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @ManyToOne
    @JoinColumn(name = "metric_id")
    private Metric metric;
    private float min;
    private float max;
    @OneToMany(mappedBy = "sla")
    private Set<SLAViolation> violations;

    public SLA() {}
    public SLA(String name, Metric metric, float min, float max) {
        this.name = name;
        this.metric = metric;
        this.min = min;
        this.max = max;
    }

    public Long getId() {
        return id;
    }
    public String getName() {
        return name;
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    public float getMin() {
        return min;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public float getMax() {
        return max;
    }

    public void setMax(float max) {
        this.max = max;
    }

}
