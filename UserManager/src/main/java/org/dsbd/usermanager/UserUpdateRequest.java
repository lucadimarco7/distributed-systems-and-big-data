package org.dsbd.usermanager;

public record UserUpdateRequest(String username, String oldPassword, String newPassword) {
}
