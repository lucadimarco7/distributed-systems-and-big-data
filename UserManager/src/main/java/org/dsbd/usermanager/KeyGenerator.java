package org.dsbd.usermanager;

import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import java.util.Base64;

public class KeyGenerator {
    private final static String key = System.getenv("KEY");

    public static SecretKeySpec generateRandomKey() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] keyBytes = new byte[64];
        secureRandom.nextBytes(keyBytes);
        return new SecretKeySpec(keyBytes, "HmacSHA512");
    }

    public static SecretKeySpec generateKey(){
        byte[] keyBytes = Base64.getDecoder().decode(key);
        return new SecretKeySpec(keyBytes, "HmacSHA512");
    }

}

