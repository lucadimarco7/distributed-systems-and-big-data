package org.dsbd.usermanager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;
    private final AuthService authService;

    public UserController(UserService userService, AuthService authService) {
        this.userService = userService;
        this.authService = authService;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleCustomExceptions(Exception ex) {
        if (ex instanceof HttpClientErrorException) {
            return ResponseEntity.status(((HttpClientErrorException) ex).getStatusCode()).body(ex.getMessage());
        }
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @PostMapping("/register")
    public ResponseEntity<String> userRegistration(@RequestBody UserRequest userRequest) {
        authService.register(userRequest);
        return ResponseEntity.ok("Registration successfully completed.");
    }

    @PostMapping("/login")
    public ResponseEntity<String> userLogin(@RequestBody UserLoginRequest userLoginRequest) {
        Long id = authService.authenticateUser(userLoginRequest);
        String token = authService.generateToken(id);
        return ResponseEntity.ok("Successfully logged in. Token: " + token);
    }

    @PutMapping("/updateUser")
    public ResponseEntity<String> userUpdate(@RequestHeader(name = "Authorization") String token, @RequestBody UserUpdateRequest userUpdateRequest) {
        Long id = authService.authorizeUser(token);
        userService.updateUser(id, userUpdateRequest);
        return ResponseEntity.ok("Update successfully completed.");
    }

    @DeleteMapping("/deleteUser")
    public ResponseEntity<String> userDelete(@RequestHeader(name = "Authorization") String token) {
        Long id = authService.authorizeUser(token);
        userService.deleteUser(id);
        return ResponseEntity.ok("User successfully deleted.");
    }


    @PostMapping("/preferences")
    public ResponseEntity<String> setUserPreferences(@RequestHeader(name = "Authorization") String token, @RequestBody UserPreferencesRequest userPreferencesRequest) {
        Long id = authService.authorizeUser(token);
        userService.setUserPreferences(id, userPreferencesRequest);
        return ResponseEntity.ok("User preferences set successfully.");
    }

    @PutMapping("/updatePreference")
    public ResponseEntity<String> updatePreference(@RequestHeader(name = "Authorization") String token, @RequestBody String request) {
        Long id = authService.authorizeUser(token);
        JSONObject requestObject = new JSONObject(request);
        for(Object preferenceId : requestObject.keySet()){
            JSONObject preference = requestObject.getJSONObject(preferenceId.toString());
            UserPreferencesRequest userPreferencesRequest = new UserPreferencesRequest(
                    preference.getString("roadSection"),
                    preference.getString("timeSlot"),
                    preference.getFloat("delayThreshold"),
                    preference.getFloat("timeThreshold")
            );
            userService.updatePreference(id, Long.parseLong(preferenceId.toString()), userPreferencesRequest);
        }
        return ResponseEntity.ok("Preference successfully updated.");
    }

    @GetMapping("/preferencesList")
    public ResponseEntity<String> userPreferences(@RequestHeader(name = "Authorization") String token) {
        Long id = authService.authorizeUser(token);
        JSONObject preferencesList = userService.preferencesUser(id);
        return ResponseEntity.ok(preferencesList.toString());
    }

    @DeleteMapping("/deletePreference")
    public ResponseEntity<String> deletePreference(@RequestHeader(name = "Authorization") String token, @RequestBody String preferenceId) {
        Long id = authService.authorizeUser(token);
        JSONObject requestObject = new JSONObject(preferenceId);
        userService.deletePreference(id, requestObject.getLong("id"));
        return ResponseEntity.ok("Preference successfully deleted.");
    }

    @PostMapping("/emails")
    public ResponseEntity<String> getEmails(@RequestHeader(name = "Authorization") String token, @RequestBody String preference) {
        authService.authorizeConsumer(token);
        JSONArray emails = userService.getEmails(preference);
        return ResponseEntity.ok(emails.toString());
    }

    @GetMapping("/roadSections")
    public ResponseEntity<String> roadSections(@RequestHeader(name = "Authorization") String token) {
        authService.authorizeUser(token);
        Set<String> roadSections = userService.getRoadSections();
        return ResponseEntity.ok(roadSections.toString());
    }

    @GetMapping("/timeSlots")
    public ResponseEntity<String> timeSlots(@RequestHeader(name = "Authorization") String token) {
        authService.authorizeUser(token);
        JSONArray timeSlots = new JSONArray(userService.getTimeSlots());
        return ResponseEntity.ok(timeSlots.toString());
    }
}
