package org.dsbd.usermanager;

public record UserPreferencesRequest(String roadSection, String timeSlot, float delayThreshold, float timeThreshold) {
}
