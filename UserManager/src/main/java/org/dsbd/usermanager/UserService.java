package org.dsbd.usermanager;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserService {
    private final IUserRepository userRepository;
    private final IUserPreferencesRepository userPreferencesRepository;
    private final PasswordEncoder passwordEncoder;
    @Value("${roadSections}")
    private Set<String> roadSections;
    @Value("${timeslot}")
    private String[] timeSlot;
    @Value("${time.threshold}")
    private float timeThreshold;

    @Autowired
    @Lazy
    public UserService(IUserRepository userRepository, IUserPreferencesRepository userPreferencesRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.userPreferencesRepository = userPreferencesRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void updateUser(Long id, UserUpdateRequest userUpdateRequest) {
        if (userUpdateRequest.newPassword().length() < 6)
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Invalid credentials.");
        User user = userRepository.findUserById(id).orElse(null);
        if (user != null) {
            if (!passwordEncoder.matches(userUpdateRequest.oldPassword(), user.getPassword()))
                throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Wrong password.");
            user.setUsername(userUpdateRequest.username());
            user.setPassword(passwordEncoder.encode(userUpdateRequest.newPassword()));
            userRepository.save(user);
        } else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "User not found.");
        }
    }

    @Transactional
    public void deleteUser(Long id) {
        User slaOptional = userRepository.findUserById(id).orElse(null);
        if(slaOptional == null){
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "User not found.");
        }
        userPreferencesRepository.deleteAllByUserId(id);
        userRepository.deleteById(id);
    }

    public void setUserPreferences(Long id, UserPreferencesRequest userPreferencesRequest) {
        checkTimeAndThreshold(userPreferencesRequest);
        UserPreferences userPreferences = userPreferencesRepository.findByRoadSectionAndTimeSlotAndDelayThresholdAndTimeThreshold(
                userPreferencesRequest.roadSection(),
                userPreferencesRequest.timeSlot(),
                userPreferencesRequest.delayThreshold(),
                userPreferencesRequest.timeThreshold()
        );
        if(userPreferences != null){
            throw new HttpClientErrorException(HttpStatus.CONFLICT, "Preference already exist.");
        }
        User user = userRepository.findUserById(id).orElse(null);
        if (user != null) {
            checkRoadSection(userPreferencesRequest);
            userPreferences = new UserPreferences(userPreferencesRequest, user);
            userPreferencesRepository.save(userPreferences);
        }
    }

    private void checkRoadSection(UserPreferencesRequest userPreferencesRequest) {
//        Set<String> topicList = kafkaTopicLister.listOfTopics();
//        String topic = userPreferencesRequest.roadSection();
//        boolean roadSectionExist = kafkaTopicLister.checkIfTopicExist(topic, topicList);
        boolean roadSectionExist = false;
        for(String roadSection : roadSections){
            if (roadSection.equals(userPreferencesRequest.roadSection())) {
                roadSectionExist = true;
                break;
            }
        }
        if (!roadSectionExist) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Road section not found.");
        }
    }

    private void checkTimeAndThreshold(UserPreferencesRequest userPreferencesRequest) {
        if (userPreferencesRequest.delayThreshold() <= 0 || userPreferencesRequest.timeThreshold() <= timeThreshold) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Invalid threshold.");
        }
        String timeSlotPreference = userPreferencesRequest.timeSlot();
        boolean isExist = false;
        for (String slot : timeSlot) {
            if (slot.equals(timeSlotPreference)) {
                isExist = true;
                break;
            }
        }
        if (!isExist) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Time slot not available! Use hh:mm format.");
        }
    }

    public JSONObject preferencesUser(Long id) {
        List<UserPreferences> preferencesList = userPreferencesRepository.findAllByUserId(id);
        if(preferencesList.isEmpty())
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "No preferences set.");
        JSONObject preferences = new JSONObject();
        for (UserPreferences userPreferences : preferencesList){
            UserPreferencesRequest userPreferencesRequest = new UserPreferencesRequest(
                    userPreferences.getRoadSection(),
                    userPreferences.getTimeSlot(),
                    userPreferences.getDelayThreshold(),
                    userPreferences.getTimeThreshold());
            Gson gson = new Gson();
            preferences.put(userPreferences.getId().toString(), gson.toJson(userPreferencesRequest));
        }
        return preferences;
    }

    public void updatePreference(Long id, Long preferenceId, UserPreferencesRequest userPreferencesRequest) {
        checkTimeAndThreshold(userPreferencesRequest);
        checkRoadSection(userPreferencesRequest);
        Optional<UserPreferences> userPreferencesOptional = userPreferencesRepository.findUserPreferencesByIdAndUserId(preferenceId, id);
        if(userPreferencesOptional.isEmpty()){
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Preference not found.");
        }
        UserPreferences userPreferences = userPreferencesOptional.get();
        userPreferences.setRoadSection(userPreferencesRequest.roadSection());
        userPreferences.setTimeSlot(userPreferencesRequest.timeSlot());
        userPreferences.setDelayThreshold(userPreferencesRequest.delayThreshold());
        userPreferences.setTimeThreshold(userPreferencesRequest.timeThreshold());
        userPreferencesRepository.save(userPreferences);
    }

    @Transactional
    public void deletePreference(Long id, Long preferenceId) {
        Optional<UserPreferences> userPreferencesOptional = userPreferencesRepository.findUserPreferencesByIdAndUserId(preferenceId, id);
        if(userPreferencesOptional.isEmpty()){
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Preference not found.");
        }
        userPreferencesRepository.deleteAllById(preferenceId);
    }

    public JSONArray getEmails(String preference) {
        JSONObject preferenceJSON = new JSONObject(preference);
        String roadSection = preferenceJSON.getString("roadSection");
        String timeSlot = preferenceJSON.getString("timeSlot");
        int delay = preferenceJSON.getInt("delay");
        int differenceTime = preferenceJSON.getInt("differenceTime");
        List<UserPreferences> preferences = userPreferencesRepository
                .findByRoadSectionAndTimeSlotAndDelayThresholdLessThanEqualAndTimeThresholdGreaterThanEqual(roadSection,timeSlot,delay,differenceTime);
        Set<Long> users = new HashSet<>();
        for (UserPreferences userPreference : preferences) {
            Long userID = userPreference.getUser().getId();
            users.add(userID);
        }
        JSONArray jsonEmails = new JSONArray();
        for (Long id : users) {
            userRepository.findUserById(id).ifPresent(user -> jsonEmails.put(user.getEmail()));
        }
        if(jsonEmails.isEmpty())
            throw new HttpClientErrorException(HttpStatus.NO_CONTENT, "");
        return jsonEmails;
    }

    public Set<String> getRoadSections() {
        if(roadSections.isEmpty())
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Road sections list is empty.");
        return roadSections;
    }
    public String[] getTimeSlots() {
        if(timeSlot.length == 0)
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Time slots empty.");
        return timeSlot;
    }
}
