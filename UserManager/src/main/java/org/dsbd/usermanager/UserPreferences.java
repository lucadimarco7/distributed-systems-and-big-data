package org.dsbd.usermanager;

import jakarta.persistence.*;

@Entity
public class UserPreferences {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String roadSection;
    private String timeSlot;
    @Column(columnDefinition = "float default 0")
    private float delayThreshold;
    @Column(columnDefinition = "float default 0")
    private float timeThreshold;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public UserPreferences() {
    }

    public UserPreferences(UserPreferencesRequest userPreferencesRequest, User user) {
        this.roadSection = userPreferencesRequest.roadSection();
        this.timeSlot = userPreferencesRequest.timeSlot();
        this.delayThreshold = userPreferencesRequest.delayThreshold();
        this.timeThreshold = userPreferencesRequest.timeThreshold();
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public float getTimeThreshold() {
        return timeThreshold;
    }

    public String getRoadSection() {
        return roadSection;
    }

    public float getDelayThreshold() {
        return delayThreshold;
    }

    public User getUser() {
        return user;
    }

    public void setRoadSection(String roadSection) {
        this.roadSection = roadSection;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public void setDelayThreshold(float delayThreshold) {
        this.delayThreshold = delayThreshold;
    }

    public void setTimeThreshold(float timeThreshold) {
        this.timeThreshold = timeThreshold;
    }
}
