package org.dsbd.usermanager;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import io.micrometer.core.instrument.Counter;
import org.springframework.web.client.HttpClientErrorException;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class AuthService {

    private static final SecretKey SECRET_KEY = KeyGenerator.generateRandomKey();
    private static final SecretKey SECRET_KEY_CONSUMER = KeyGenerator.generateKey();
    private static final long EXPIRATION_TIME = 3600000;
    private final IUserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    private final Counter registrationCounter;
    private final Counter loginCounter;

    @Autowired
    @Lazy
    public AuthService(IUserRepository userRepository, PasswordEncoder passwordEncoder, MeterRegistry registry) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.registrationCounter = Counter.builder("registrations")
                .description("Number of registrations.")
                .register(registry);
        this.loginCounter = Counter.builder("logins")
                .description("Number of logins.")
                .register(registry);
    }

    public void register(UserRequest userRequest) {
        if(userRequest.username().isEmpty() || !verifyEmail(userRequest.email()) || userRequest.password().length() < 6)
            throw new HttpClientErrorException(HttpStatus.CONFLICT, "Invalid credentials.");
        if (userRepository.existsByUsername(userRequest.username())) {
            throw new HttpClientErrorException(HttpStatus.CONFLICT, "Username already registered.");
        }
        if (userRepository.existsByEmail(userRequest.email())) {
            throw new HttpClientErrorException(HttpStatus.CONFLICT, "E-mail already registered.");
        }
        User user = new User(userRequest.username(), userRequest.email(), userRequest.password());
        encodePassword(user, userRequest);
        userRepository.save(user);
        registrationCounter.increment();
    }

    private boolean verifyEmail (String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public Long authenticateUser(UserLoginRequest userLoginRequest) {
        Optional<User> userOptional = userRepository.findByUsername(userLoginRequest.username());
        User user;
        if (userOptional.isPresent()) {
            user = userOptional.get();
            if (!passwordEncoder.matches(userLoginRequest.password(), user.getPassword()))
                throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Wrong password.");
        } else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "User not found.");
        }
        return user.getId();
    }

    public Long authorizeUser(String token) {
        if (token.startsWith("Bearer ")) {
            token = token.substring(7);
        }

        Jws<Claims> jws = Jwts.parser().verifyWith(SECRET_KEY).build().parseSignedClaims(token);
        Claims claims = jws.getPayload();

        Date expiration = claims.getExpiration();
        if (isTokenExpired(expiration))
            throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Token expired.");

        String tokenId = claims.get("id").toString();
        Long id = Long.parseLong(tokenId);
        if (!userRepository.existsById(id))
            throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Unauthorized.");
        loginCounter.increment();
        return id;
    }

    public String generateToken(Long id) {
        return Jwts.builder()
                .claim("id", id)
                .expiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SECRET_KEY)
                .compact();
    }

    private boolean isTokenExpired(Date expiration) {
        return expiration.before(new Date());
    }

    private void encodePassword(User user, UserRequest userRequest) {
        user.setPassword(passwordEncoder.encode(userRequest.password()));
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public void authorizeConsumer(String token) {
        if (token.startsWith("Bearer ")) {
            token = token.substring(7);
        }
        Jwts.parser().verifyWith(SECRET_KEY_CONSUMER).build().parseSignedClaims(token);
    }
}
