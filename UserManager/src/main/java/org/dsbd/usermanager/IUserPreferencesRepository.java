package org.dsbd.usermanager;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface IUserPreferencesRepository extends JpaRepository<UserPreferences, Long> {
    void deleteAllByUserId(Long id);
    void deleteAllById(Long id);
    Optional<UserPreferences> findUserPreferencesById(Long id);
    Optional<UserPreferences> findUserPreferencesByIdAndUserId(Long preferenceId, Long id);
    List<UserPreferences> findAllByUserId(Long id);

    UserPreferences findByRoadSectionAndTimeSlotAndDelayThresholdAndTimeThreshold(String roadSection, String timeSlot, float delayThreshold, float timeThreshold);
    List<UserPreferences> findByRoadSectionAndTimeSlotAndDelayThresholdLessThanEqualAndTimeThresholdGreaterThanEqual(String roadSection, String timeSlot, float delayThreshold, float timeThreshold);

}
