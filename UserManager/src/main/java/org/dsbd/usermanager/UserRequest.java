package org.dsbd.usermanager;

public record UserRequest(String username, String email, String password) {
}
