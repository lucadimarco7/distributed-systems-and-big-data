package org.dsbd.usermanager;

public record UserLoginRequest(String username, String password) {
}
