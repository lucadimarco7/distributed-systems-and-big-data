package org.dsbd.producer;

public record Coordinates(Double lat, Double lon) {
    @Override
    public String toString(){
        return lat + "," + lon;
    }
}
