package org.dsbd.producer;

public record RoadSection(String startCity, String endCity) {

}
