package org.dsbd.producer;

public record TomTomResponse(String startCity, String endCity, int travelTimeInSeconds,
                             String departureTime, int noTrafficTravelTimeInSeconds, int differenceTime) {
}