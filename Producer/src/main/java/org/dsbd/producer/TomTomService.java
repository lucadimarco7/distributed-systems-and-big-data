package org.dsbd.producer;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

@Service
public class TomTomService {
    private final RestTemplate restTemplate = new RestTemplate();
    private final String key_api = System.getenv("KEY_API");
    private final Map<String, Coordinates> coordinatesMap = new HashMap<>();

    private static TomTomResponse getTomTomResponse(String startCity, String endCity, String response, int differenceTime) {
        JSONObject jsonResponse = new JSONObject(response);
        JSONArray routes = jsonResponse.getJSONArray("routes");
        JSONObject route = routes.getJSONObject(0);
        JSONObject summary = route.getJSONObject("summary");

        int travelTimeInSeconds = summary.getInt("travelTimeInSeconds");
        int noTrafficTravelTimeInSeconds = summary.getInt("noTrafficTravelTimeInSeconds");
        String departureTime = summary.getString("departureTime");

        return new TomTomResponse(startCity, endCity, travelTimeInSeconds, departureTime, noTrafficTravelTimeInSeconds, differenceTime);
    }

    public String getTomTomData(RoadSection roadSection, String timeSlot) {
        String startCity = roadSection.startCity();
        String endCity = roadSection.endCity();
        Coordinates startCoordinates = coordinatesMap.get(startCity);
        Coordinates endCoordinates = coordinatesMap.get(endCity);

        LocalDate day = LocalDate.now();
        int differenceTime = getDifferenceTime(timeSlot);
        if (differenceTime < 0){
            day = day.plusDays(1);
            differenceTime += 24 * 60;
        }
        String departAt = day + "T" + timeSlot + ":00";
        String url = "https://api.tomtom.com/routing/1/calculateRoute/" + startCoordinates.toString() + ":" + endCoordinates.toString() + "/json?departAt=" + departAt + "&routeRepresentation=summaryOnly&travelMode=bus&computeTravelTimeFor=all&routeType=shortest&key=" + key_api;
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        if (response.getStatusCode() != HttpStatusCode.valueOf(200)) {
            System.out.println(response);
            throw new RestClientException(response.getStatusCode().toString());
        }
        System.out.println(response.getBody());
        TomTomResponse tomTomResponse = getTomTomResponse(startCity, endCity, response.getBody(), differenceTime);
        Gson gson = new Gson();
        return gson.toJson(tomTomResponse);
    }

    public boolean getCoordinates(String city) {
        String url = "https://api.tomtom.com/search/2/geocode/" + city + ".json?key=" + key_api;

        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        if (response.getBody() == null){
            throw new RestClientException("Response body is empty!");
        }
        JSONObject jsonObject = new JSONObject(response.getBody());
        JSONArray results = jsonObject.getJSONArray("results");
        JSONObject firstResult = results.getJSONObject(0);
        JSONObject position = firstResult.getJSONObject("position");
        double lat = position.getDouble("lat");
        double lon = position.getDouble("lon");

        Coordinates coordinates = new Coordinates(lat, lon);
        coordinatesMap.put(city, coordinates);
        return true;
    }

    private static int getDifferenceTime(String timeSlotString) {
        LocalDateTime timestampNow = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime timeSlot = LocalTime.parse(timeSlotString, formatter);
        LocalDateTime timeSlotDateTime = timestampNow.toLocalDate().atTime(timeSlot);
        return (int) ChronoUnit.MINUTES.between(timestampNow, timeSlotDateTime);
    }
}
