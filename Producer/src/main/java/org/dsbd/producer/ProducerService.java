package org.dsbd.producer;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import jakarta.annotation.PostConstruct;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

@Service
public class ProducerService {
    private final ArrayList<RoadSection> roadSections = new ArrayList<>();
    private final TomTomService tomTomService;
    private final KafkaTemplate<String, String> kafkaTemplate;
    @Value("${roadSections}")
    private String[] roadSectionNames;
    @Value("${topic}")
    private String topicName;
    @Value("${timeslot}")
    private String[] timeSlots;
    private Boolean isCreated = false;
    private final Timer responseTimer;

    public ProducerService(KafkaTemplate<String, String> kafkaTemplate, TomTomService tomTomService, MeterRegistry registry) {
        this.kafkaTemplate = kafkaTemplate;
        this.tomTomService = tomTomService;
        this.responseTimer = Timer.builder("response.time.producer")
                .description("Response time to produce all the data.")
                .register(registry);
    }


    @PostConstruct
    public void init() {
        boolean success = false;
        for (String name : roadSectionNames) {
            String[] parts = name.split("-");
            String startCity = parts[0];
            String endCity = parts[1];
            try {
                success = tomTomService.getCoordinates(startCity);
                if (!success) {
                    System.out.println("Error obtaining coordinates.");
                }
                success = tomTomService.getCoordinates(endCity);
                if (!success) {
                    System.out.println("Error obtaining coordinates.");
                }
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            RoadSection roadSection = new RoadSection(startCity, endCity);
            roadSections.add(roadSection);
        }
        isCreated = true;
    }

    @Scheduled(fixedRateString = "${sending.time}")
    public void sendPeriodically() {
        if(isCreated) {
            responseTimer.record(() -> {
                for (RoadSection roadSection : roadSections) {
                    for (String timeSlot : timeSlots) {
                        try {
                            String tomtomData = tomTomService.getTomTomData(roadSection, timeSlot);
                            kafkaTemplate.send(new ProducerRecord<>(topicName, roadSection.toString(), tomtomData));
                            Thread.sleep(1000);
                        } catch (RestClientException ex) {
                            System.out.println("Error request HTTP: " + ex.getMessage());
                        } catch (Exception ex) {
                            System.out.println(ex.getMessage());
                        }
                    }
                }
            });
        }
    }
}
